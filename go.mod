module gitlab.com/remotejob/k8s-helm-grpc-mysql

go 1.19

require (
	github.com/google/uuid v1.3.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.14.0
)

require (
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/oklog/run v1.1.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
)

require (
	github.com/cenk/backoff v2.2.1+incompatible
	github.com/go-sql-driver/mysql v1.7.0
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/oklog/oklog v0.3.2
	golang.org/x/net v0.2.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221114212237-e4508ebdbee1 // indirect
	google.golang.org/grpc v1.50.1 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
